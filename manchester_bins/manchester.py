#!/usr/bin/env python3

import aiohttp
import datetime
import hashlib


async def post_request_result_text(url, payload={}, headers={}):
    async with aiohttp.ClientSession() as session:
        async with session.post(url, data=payload, headers=headers) as response:
            return await response.text()


async def get_request_result_text(url, payload={}, headers={}):
    async with aiohttp.ClientSession() as session:
        async with session.get(url, data=payload, headers=headers) as response:
            return await response.text()


async def get_request_auth_key():
    async with aiohttp.ClientSession() as session:
        async with session.get(
            "https://manchester.form.uk.empro.verintcloudservices.com/api/citizen?archived=Y&preview=false&locale=en"
        ) as response:
            return response.headers['Authorization']


async def get_street_address_options(post_code):
    async with aiohttp.ClientSession() as session:
        async with session.post(
            "https://manchester.form.uk.empro.verintcloudservices.com/api/custom?action=widget-property-search&actionedby=location_search_property&loadform=true&access=citizen&locale=en",
            json={
                "name": "sr_bin_coll_day_checker",
                "data": {
                    "addressnumber": "",
                    "streetname": "",
                    "postcode": post_code
                },
                "email": "",
                "caseid": "",
                "xref": "",
                "xref1": "",
                "xref2": ""
            },
            headers={
                "Authorization": await get_request_auth_key()
            }
        ) as response:
            
            result_json = await response.json()
            found = []
            for address in result_json['data']['prop_search_results']:
                found.append({"id": address['value'], "address": address['label']})
            return found


async def get_street_address_uprn(address_selection_id):
    async with aiohttp.ClientSession() as session:
        async with session.post(
            "https://manchester.form.uk.empro.verintcloudservices.com/api/custom?action=retrieve-property&actionedby=_KDF_optionSelected&loadform=true&access=citizen&locale=en",
            json={
                "caseid": "",
                "data": {
                    "object_id": address_selection_id
                },
                "email": "",
                "name": "sr_bin_coll_day_checker",
                "xref": "",
                "xref1": "",
                "xref2": ""
            },
            headers={
                "Authorization": await get_request_auth_key()
            }
        ) as response:
            
            result_json = await response.json()
            return result_json['data']['UPRN']


def recover_scheduled_bin_collection_date(scheduled_str):
    date_str = scheduled_str.split(" ")[0]
    return datetime.datetime.strptime(date_str, "%d/%m/%Y").date()


async def get_next_collection_dates(uprn):
    today = datetime.date.today()
    future = today + datetime.timedelta(days=8)

    async with aiohttp.ClientSession() as session:
        async with session.post(
            "https://manchester.form.uk.empro.verintcloudservices.com/api/custom?action=bin_checker-get_bin_col_info&actionedby=_KDF_custom&loadform=true&access=citizen&locale=en",
            json={
                "name":"sr_bin_coll_day_checker",
                "data":{
                    "uprn": uprn,
                    "nextCollectionFromDate": today.strftime('%Y-%m-%d'),
                    "nextCollectionToDate": future.strftime('%Y-%m-%d')
                },
                "email":"",
                "caseid":"",
                "xref":"",
                "xref1":"",
                "xref2":""
            },
            headers={
                "Authorization": await get_request_auth_key()
            }
        ) as response:
            
            result_json = await response.json()
            return {
                "black_bin": recover_scheduled_bin_collection_date(result_json['data']['ahtm_dates_black_bin']),
                "brown_bin": recover_scheduled_bin_collection_date(result_json['data']['ahtm_dates_brown_commingled_bin']),
                "blue_bin": recover_scheduled_bin_collection_date(result_json['data']['ahtm_dates_blue_pulpable_bin']),
                "green_bin": recover_scheduled_bin_collection_date(result_json['data']['ahtm_dates_green_organic_bin'])
            }


class ManchesterBinCollectionApi:
    
    def __init__(self, street_address_id):
        self.street_address_id = street_address_id
    
    
    async def connect(self):
        try:
            await self.__update()
        except:
            self.failed = True
        else:
            self.failed = False


    def __convert_bin_key_to_name(self, bin_key):
        return bin_key.replace("_", " ").capitalize()


    async def __update(self):
        next_collection = await get_next_collection_dates(self.street_address_id)

        bins = {
            "black_bin": {
                "name": "Black bin",
                "date": next_collection['black_bin']
            }
        }

        for bin_key in next_collection.keys():
            bins[bin_key] = {
                "name": self.__convert_bin_key_to_name(bin_key),
                "date": next_collection[bin_key]
            }
        
        self.bin_data = bins
        self.last_updated = datetime.datetime.now()
    

    async def update(self):
        now = datetime.datetime.now()

        if now.strftime("%d") != self.last_updated.strftime("%d"):
            await self.__update()
            return True
        else:
            return False
    

    def get_bin_keys(self):
        found = []
        for bin_key in self.bin_data.keys():
            found.append(bin_key)
        return found


    def get_bin_name(self, bin_key):
        return self.bin_data[bin_key]['name']
    

    def get_bin_collection_date(self, bin_key):
        return self.bin_data[bin_key]['date']
    

    def get_bin_unique_id(self, bin_key):
        unique_name = str(self.street_address_id)+"__"+bin_key
        return hashlib.md5(str(unique_name).encode("utf-8")).hexdigest()


    def get_next_collected_bin_keys(self):
        found = []

        next_date = self.get_next_collection_date()
        for bin_key in self.bin_data.keys():
            if self.get_bin_collection_date(bin_key) == next_date:
                found.append(bin_key)
        
        return found


    def get_next_collection_date(self):
        today = datetime.date.today()

        lowest_diff = 100
        lowest_date = None
        for bin_key in self.bin_data.keys():
            diff = self.get_bin_collection_date(bin_key) - today

            if diff.days < lowest_diff:
                lowest_diff = diff.days
                lowest_date = self.get_bin_collection_date(bin_key)
        
        return lowest_date
    

    def get_days_until_next_collection(self):
        today = datetime.date.today()
        diff = self.get_next_collection_date() - today
        return diff.days